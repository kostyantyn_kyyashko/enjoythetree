<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
 *
 * cli app
*/
require_once 'TreeDb.php';

if (isset($argv[1])) {
    $action = $argv[1];
}
else {
    echo "Action empty\n";
    die();
}

$db = new TreeDb();
if (!method_exists($db, $action)) {
    echo "Action incorrect\n";
    die();
}
//addNode. Format: php test.php addNode <title> <parentId>
if ($action == 'addNode') {
    $title = isset($argv[2])?$argv[2]:'';
    $parentNodeId = isset($argv[3])?$argv[3]:1;
    if (!$db->checkNodeId($parentNodeId)) {
        echo "Parent nodeId {$parentNodeId} not exists\n";
        die();
    }
    $insertedId = $db->addNode($title, $parentNodeId);
    echo "Node {$title} has been added with id #{$insertedId}\n";
    die();
}

//deleteNode. Format: php test.php deleteNode <nodeId>
if ($action == 'deleteNode') {
    $nodeId = isset($argv[2])?$argv[2]:false;
    if (!$nodeId) {
        echo "nodeId not present in request\n";
        die();
    }
    if (!$db->checkNodeId($nodeId)) {
        echo "nodeId {$nodeId} not exists\n";
        die();
    }
    $db->deleteNode($nodeId);
    echo "Node id #{$nodeId} has been deleted\n";
    die();
}

//moveNode. Format: php test.php modeNode <nodeId> <newParentId>
if ($action == 'moveNode') {
    $nodeId = isset($argv[2])?$argv[2]:false;
    if (!$nodeId) {
        echo "nodeId not present in request\n";
        die();
    }
    $parentNodeId = isset($argv[3])?$argv[3]:false;
    if (!$nodeId) {
        echo "parentNodeId not present in request\n";
        die();
    }
    if (!$db->checkNodeId($nodeId)) {
        echo "nodeId {$nodeId} not exists\n";
        die();
    }
    if (!$db->checkNodeId($parentNodeId)) {
        echo "parentNodeId {$nodeId} not exists\n";
        die();
    }
    $db->moveNode($nodeId, $parentNodeId);
    echo "Node id #{$nodeId} has been moved to {$parentNodeId} parent\n";
}

//renameNode
if ($action == 'renameNode') {
    $title = isset($argv[2])?$argv[2]:'';
    $nodeId = isset($argv[3])?$argv[3]:1;
    if (!$db->checkNodeId($nodeId)) {
        echo "nodeId {$nodeId} not exists\n";
        die();
    }
    $insertedId = $db->renameNode($title, $nodeId);
    echo "Node id #{$nodeId} has been renamed to {$title}\n";
    die();
}








