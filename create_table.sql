DROP TABLE IF EXISTS category;
CREATE TABLE IF NOT EXISTS category (
  node_id smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  lft smallint(5) UNSIGNED NOT NULL,
  rgt smallint(5) UNSIGNED NOT NULL,
  parent_id smallint(5) UNSIGNED NOT NULL,
  level smallint(5) UNSIGNED NOT NULL,
  title varchar(20) DEFAULT '',
  PRIMARY KEY (node_id),
  INDEX IDX_category_lft (lft),
  INDEX IDX_category_rgt (rgt),
  INDEX IDX_category_title (title)
)
  ENGINE = INNODB;

INSERT INTO `category` (`node_id`,`lft`,`rgt`,`parent_id`,`level`,`title`) VALUES (1, 1, 2, 0, 0, 'root');