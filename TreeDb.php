<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
require_once 'Db.php';
class TreeDb extends Db
{
    /**
     * @param $nodeId
     * @return bool|string
     */
    public function checkNodeId ($nodeId)
    {
        $sql = "SELECT `node_id` FROM `category` WHERE `node_id` = {$nodeId}";
        try {
            $result = $this->selectValue($sql);
        }
        catch (Exception $e) {
            echo "Error!\n";
            echo $e->getMessage() . "\n";
        }
        return $result;
    }

    /**
     * @param string $title
     * @param int $parentNodeId
     * @return bool|string
     */
    public function addNode ($title = '', $parentNodeId = 1)
    {
        $sql = <<<sql
CALL enjoy_the_tree('insert', NULL, $parentNodeId, :title)
sql;
        $data = ['title' => $title];
        try {
            $result = $this->selectValue($sql, $data);
        }
        catch (Exception $e) {
            echo "Error!\n";
            echo $e->getMessage() . "\n";
        }
        return $result;
    }

    /**
     * @param $nodeId
     * @return bool|string
     */
    public function deleteNode ($nodeId)
    {
        $sql = <<<sql
CALL enjoy_the_tree('delete', {$nodeId}, NULL, NULL)
sql;
        try {
            $this->sqlPrepareAndExecute($sql);
        }
        catch (Exception $e) {
            echo "Error!\n";
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * @param $nodeId
     * @param $newParentNodeId
     */
    public function moveNode ($nodeId, $newParentNodeId)
    {
        $sql = <<<sql
CALL enjoy_the_tree('move', {$nodeId}, {$newParentNodeId}, '')
sql;
        try {
            $this->sqlPrepareAndExecute($sql);
        }
        catch (Exception $e) {
            echo "Error!\n";
            echo $e->getMessage() . "\n";
        }
    }

    public function renameNode ($newTitle, $nodeId)
    {
        $sql = <<<sql
UPDATE `category` SET `title` = :title WHERE `node_id` = :node_id 
sql;
        $data = [
            'node_id' => $nodeId,
            'title' => $newTitle,
        ];

        try {
            $this->sqlPrepareAndExecute($sql, $data);
        }
        catch (Exception $e) {
            echo "Error!\n";
            echo $e->getMessage() . "\n";
        }
    }

}
