<?php
class Db{

    /**
     * Принцип: при создании объекта сразу соединяемся с БД, получаем PDO handler
     * и далее его используем для исполнения SQL
     */

    /**
     * Параметры соединения с БД
     */
    private $dbHost = 'localhost';
    private $dbName = 'nested';
    private $dbUser = 'konst20';
    private $dbPass = 'rbzirj25';

    /**
     * Идентификатор соединения с БД
     */
    protected $pdo;

    /**
     * При создании объекта сразу соединяемся с БД, handler потом используется методами класса
     */
    public function __construct(){

        $this->pdo = $this->getDbHandler();
        $this->pdo->exec('SET NAMES UTF8');
    }

    /**
     * @static
     * @param  $dbHost
     * @param  $dbName
     * @param  $dbUser
     * @param  $dbPass
     * @return PDO
     */
    private function dbConnect($dbHost, $dbName, $dbUser, $dbPass) {
        try {
           $dbHandler = new PDO('mysql:host='.$dbHost.';dbname='.$dbName.'', $dbUser, $dbPass);
        }
        catch(PDOException $e) {
            echo 'Db error '.$e->getMessage();
            die();
        }

        return $dbHandler;
    }

    /**
     * @return PDO
     */
    private function getDbHandler(){
        $handler = $this->dbConnect($this->dbHost, $this->dbName, $this->dbUser, $this->dbPass);
        return $handler;
    }

    /**
     * Подготовка и выполнение SQL-запросов с параметрами, см. примеры
     * @throws Exception
     * @param  $sql
     * @param array $data
     * @return PDOStatement
     */
    public function sqlPrepareAndExecute($sql, $data = array()){

        $pdo = $this->pdo;

        $st = $pdo->prepare($sql);// $st - PDOStatement
        $result = $st->execute($data);
        if(!$result){
            throw new Exception('DB error: '.implode($st->errorInfo(), ' '));
        }

        return $st;
    }

    /**
     * @param $sql
     * @param array $data
     * @return array|bool
     * @throws Exception
     */
    protected function selectAll($sql, $data = array()){
        $st = $this->sqlPrepareAndExecute($sql, $data);
        $rawArray = $st->fetchAll(PDO::FETCH_ASSOC);
        if(!is_array($rawArray) || count($rawArray) == 0){
            return false;
        }
        return $rawArray;
    }

    /**
     * ф-я применяется, для выборки одной записи,
     * возвращает ассоциативный массив, ключи - имена полей таблицы, значения - значения полей таблицы
     * если в выборке несколько записей, можно указать, какая именно по счету интересует, указав параметр $rec_num
     * array(
     * [field1] => $value1,
     * [field2] => $value2
     * ......
     * )
     * @param $sql
     * @param array $data
     * @param int $recNum
     * @return bool
     * @
     */
    protected function selectOneRecord($sql, $data = array(), $recNum = 0){
        $st = $this->sqlPrepareAndExecute($sql, $data);
        $rawArray = $st->fetchAll(PDO::FETCH_ASSOC);
        if(!is_array($rawArray) || count($rawArray) == 0){
            return false;
        }
        if($recNum > count($rawArray) - 1){
            return false;
        }
        return $rawArray[$recNum];
    }

    /**
     * Ф-я используется, если требуется обеспечить уникальность каждого элемента выборки
     * Ф-я преобразует результат выборки PDO из БД в ассоциативный массив вида
     * [id0] => array(id0, ...)
     * [id1] => array(id1, ...)
     * [id2] => array(id2, ...)
     * где каждый вложенный массив - строка (запись) таблицы
     * возвращает false, если число записей в выборке == 0,
     * либо массив
     * @param $sql
     * @param $data
     * @param string $idFieldName
     * @return array|bool
     */
    protected function selectAllWithId($sql, $data = array(), $idFieldName = 'id'){
        $st = $this->sqlPrepareAndExecute($sql, $data);
        $rawArray = $st->fetchAll(PDO::FETCH_ASSOC);
        if(!is_array($rawArray) || count($rawArray) == 0){
            return false;
        }
        $outerArray = array();
        foreach($rawArray as $item){
            $outerArray[trim($item[$idFieldName])] = $item;
        }

        return $outerArray;
    }

    /**
     * Ф-я преобразует результат выборки PDO из БД в простой массив вида
     * [0] => value0
     * [1] => value1
     * [2] => value2
     * Применяется при выборке одного поля (столбца) из БД
     * возвращает false, если число записей в выборке == 0,
     * либо массив
     * @param $sql
     * @param $data
     * @param bool $fieldName
     * @param int $fieldIndex
     * @return array|bool
     */
    protected function select_column($sql, $data = array(), $fieldName = false, $fieldIndex = 0){
        $st = $this->sqlPrepareAndExecute($sql, $data);
        $rawArray = $st->fetchAll(PDO::FETCH_NUM);
        if(!is_array($rawArray) || count($rawArray) == 0){
            return false;
        }

        $outerArray = array();

        if(!$fieldName){
            $rawArray = array_values($rawArray);
            foreach($rawArray as $item){
                $outerArray[] = $item[$fieldIndex];
            }

        }
        else{
            foreach($rawArray as $item){
                $outerArray[] = $item[$fieldName];
            }
        }

        return $outerArray;
    }

    /**
     * выборка одного значения
     * @param $sql
     * @param array $data
     * @return bool|string
     */
    protected function selectValue($sql, $data = array()){
        try {
            $st = $this->sqlPrepareAndExecute($sql, $data);
        }
        catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
        $rawValue = $st->fetchColumn();
        if(!$rawValue || is_null($rawValue) || empty($rawValue)){
            return false;
        }
        return $rawValue;
    }

}
 
