<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
require_once 'Db.php';

$createTableSql = file_get_contents('create_table.sql');
$createViewSql = file_get_contents('create_view.sql');
$createProcedureSql = file_get_contents('create_procedure.sql');
$db = new Db();
try {
    $db->sqlPrepareAndExecute($createTableSql);
    $db->sqlPrepareAndExecute($createViewSql);
    $db->sqlPrepareAndExecute($createProcedureSql);
    echo "ok\n";
}
catch (Exception $e) {
    echo "Error!\n";
    echo $e->getMessage() . "\n";
}
