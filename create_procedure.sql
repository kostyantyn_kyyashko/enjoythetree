DROP PROCEDURE IF EXISTS enjoy_the_tree;
CREATE PROCEDURE enjoy_the_tree(
  IN ptask_type VARCHAR(10),
  IN pnode_id INT,
  IN pparent_id INT,
  IN ptitle VARCHAR(20)
)
  BEGIN

    DECLARE new_lft, plevel, new_rgt, width, has_leafs, superior, superior_parent, old_lft, old_rgt, parent_rgt, subtree_size SMALLINT;

    CASE ptask_type

      WHEN 'insert' THEN

      SELECT rgt, level INTO new_lft, plevel FROM category WHERE node_id = pparent_id;
      UPDATE category SET rgt = rgt + 2 WHERE rgt >= new_lft;
      UPDATE category SET lft = lft + 2 WHERE lft > new_lft;
      INSERT INTO category (lft, rgt, parent_id, title, level) VALUES (new_lft, (new_lft + 1), pparent_id, ptitle, plevel + 1);
      SELECT LAST_INSERT_ID();

      WHEN 'delete' THEN

      SELECT lft, rgt, (rgt - lft), (rgt - lft + 1), parent_id
      INTO new_lft, new_rgt, has_leafs, width, superior_parent
      FROM category WHERE node_id = pnode_id;

      IF (has_leafs = 1) THEN
        DELETE FROM category WHERE lft BETWEEN new_lft AND new_rgt;
        UPDATE category SET rgt = rgt - width WHERE rgt > new_rgt;
        UPDATE category SET lft = lft - width WHERE lft > new_rgt;
      ELSE
        DELETE FROM category WHERE lft = new_lft;
        UPDATE category SET rgt = rgt - 1, lft = lft - 1, parent_id = superior_parent
        WHERE lft BETWEEN new_lft AND new_rgt;
        UPDATE category SET rgt = rgt - 2 WHERE rgt > new_rgt;
        UPDATE category SET lft = lft - 2 WHERE lft > new_rgt;
      END IF;

      WHEN 'move' THEN

      IF (pnode_id != pparent_id) THEN
        CREATE TEMPORARY TABLE IF NOT EXISTS working_category
        (
          `node_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
          `lft` SMALLINT(5) UNSIGNED DEFAULT NULL,
          `rgt` SMALLINT(5) UNSIGNED DEFAULT NULL,
          `parent_id` SMALLINT(5) UNSIGNED NOT NULL,
          `level` SMALLINT(5) UNSIGNED DEFAULT NULL,
          `title` VARCHAR(20) DEFAULT '',
          PRIMARY KEY (`node_id`)
        );

        -- put subtree into temporary table
        INSERT INTO working_category (node_id, lft, rgt, parent_id, level, title)
          SELECT cat1.node_id,
            (cat1.lft - (SELECT MIN(lft) FROM category WHERE node_id = pnode_id)) AS lft,
            (cat1.rgt - (SELECT MIN(lft) FROM category WHERE node_id = pnode_id)) AS rgt,
            cat1.parent_id,
            cat1.level,
            cat1.title
          FROM category AS cat1, category AS t2
          WHERE cat1.lft BETWEEN t2.lft AND t2.rgt
                AND t2.node_id = pnode_id;

        DELETE FROM category WHERE node_id IN (SELECT node_id FROM working_category);

        SELECT rgt INTO parent_rgt FROM category WHERE node_id = pparent_id;
        SET subtree_size = (SELECT (MAX(rgt) + 1) FROM working_category);

        -- make a gap in the tree
        UPDATE category
        SET lft = (CASE WHEN lft > parent_rgt THEN lft + subtree_size ELSE lft END),
          rgt = (CASE WHEN rgt >= parent_rgt THEN rgt + subtree_size ELSE rgt END)
        WHERE rgt >= parent_rgt;

        INSERT INTO category (node_id, lft, rgt, parent_id, level, title)
          SELECT node_id, lft + parent_rgt, rgt + parent_rgt, parent_id, level, title
          FROM working_category;

        -- close gaps in tree
        UPDATE category
        SET lft = (SELECT COUNT(*) FROM vw_lftrgt AS v WHERE v.lft <= category.lft),
          rgt = (SELECT COUNT(*) FROM vw_lftrgt AS v WHERE v.lft <= category.rgt);

        DELETE FROM working_category;
        UPDATE category SET parent_id = pparent_id WHERE node_id = pnode_id;
      END IF;

    END CASE;

  END